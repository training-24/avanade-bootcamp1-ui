import { computed, Injectable, signal } from '@angular/core';

interface Auth {
  token: string;
  role: 'admin' | 'guest';
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isAuth = signal<Auth | null>(null)
  isLogged = computed(() => {
    return !!this.isAuth()
  })
  role = computed(() => {
    return this.isAuth()?.role
  })

  signIn() {
      // http
    this.isAuth.set({ token: '123', role: 'admin' })
    // this.isLogged.set(true)
  }
  signOut() {
    this.isAuth.set(null)
  }

}
