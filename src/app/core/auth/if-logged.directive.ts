import {
  Directive,
  effect,
  ElementRef,
  HostBinding,
  inject, input,
  Input,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { AuthService } from './auth.service';

@Directive({
  selector: '[appIfLogged]',
  standalone: true
})
export class IfLoggedDirective {
  requiredRole = input<'admin' | 'guest'>('guest', { alias: 'appIfLogged'})

  el = inject(ElementRef)
  tpl = inject(TemplateRef)
  view = inject(ViewContainerRef)
  authSrv = inject(AuthService)

  constructor() {
    effect(() => {
      const isLogged = this.authSrv.isLogged()
      const userRole = this.authSrv.role()
      if (isLogged && userRole === this.requiredRole()) {
        this.view.createEmbeddedView(this.tpl)
      } else {
        this.view.clear()
      }
    });
  }

}
