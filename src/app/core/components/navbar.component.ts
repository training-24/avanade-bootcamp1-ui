import { Component, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { IfLoggedDirective } from '../auth/if-logged.directive';
import { MyRouterLinkDirective } from '../../shared/directives/my-router-link.directive';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    IfLoggedDirective,
    MyRouterLinkDirective,
    RouterLink
  ],
  template: `

    <div class="flex gap-3">
      <button routerLink="demo1" appMyRouterLink="bg-sky-400">Directive 1</button>
      <button routerLink="demo2" appMyRouterLink="bg-sky-400">Directive 2</button>
      <button routerLink="demo3" appMyRouterLink="bg-sky-400">Pipes</button>
      <button routerLink="demo4" appMyRouterLink="bg-sky-400">demo4</button>
      <button routerLink="demo5" appMyRouterLink="bg-sky-400">demo5</button>
      <button routerLink="demo6" appMyRouterLink="bg-sky-400">demo6</button>

      <button (click)="authSrv.signIn()">login</button>
      <button *appIfLogged="'admin'" (click)="authSrv.signOut()">logout</button>
    </div>
  `,
  styles: ``
})
export class NavbarComponent {
  authSrv = inject(AuthService)
}
