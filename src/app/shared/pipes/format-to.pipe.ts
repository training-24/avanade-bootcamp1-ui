import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatTo',
  standalone: true
})
export class FormatToPipe implements PipeTransform {

  transform(value: number, format?: 'mb' | 'gb'): string {
    switch (format) {
      case 'mb': return `${value / 1000000}Mb`;
      case 'gb': return `${value / 1000000000}Gb`;
      default:
        return value.toString();
    }

  }

}
