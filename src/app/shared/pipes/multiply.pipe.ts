import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multiply',
  standalone: true
})
export class MultiplyPipe implements PipeTransform {

  transform(value: number): number {
    console.log('transform multiply')
    return value * 2;
  }

}
