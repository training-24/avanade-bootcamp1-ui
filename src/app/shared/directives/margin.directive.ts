import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appMargin]',
  standalone: true
})
export class MarginDirective {
  @Input({ alias: 'appMargin' })
  size: 'sm' | 'md' | 'xl' = 'sm';

  @HostBinding() get class() {
    switch(this.size){
      case 'sm': return 'm-3 sm:m-12';
      case 'md': return 'm-6 sm:m-16'
      case 'xl': return 'm-9 sm:m-24'
    }
  }

}
