import {
  computed,
  Directive,
  effect,
  ElementRef,
  HostBinding,
  inject,
  input,
  Input, numberAttribute,
  OnChanges, Renderer2,
  signal
} from '@angular/core';

@Directive({
  selector: '[appBorder]',
  standalone: true
})
export class BorderDirective  {
  appBorder = input(12, { transform: numberAttribute})
  type = computed(() => this.appBorder() > 5 ? 'solid' : 'dashed');

  el = inject(ElementRef)
  renderer = inject(Renderer2)

  constructor() {
    effect(() => {
      this.renderer.setStyle(
        this.el.nativeElement,
        'border',
        `${this.appBorder()}px ${this.type()} red`
      )
    });

  }

}
