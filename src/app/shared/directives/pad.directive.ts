import { Directive, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appPad]',
  standalone: true
})
export class PadDirective {
  @Input({ alias: 'appPad' })
  size: 'sm' | 'md' | 'xl' = 'sm';

  @HostBinding('style.padding') get pad() {
    switch(this.size){
      case 'sm': return '10px 0';
      case 'md': return '30px 0';
      case 'xl': return '50px 0';
    }
  }




}

