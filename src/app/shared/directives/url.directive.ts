import { Directive, ElementRef, HostListener, inject, Input } from '@angular/core';

@Directive({
  selector: '[url]',
  standalone: true
})
export class UrlDirective {
  @Input() url: string | undefined;

  @HostListener('click')
  clickMe() {
    window.open(this.url)
  }


}
