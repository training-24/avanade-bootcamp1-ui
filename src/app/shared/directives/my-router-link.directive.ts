import { Directive, ElementRef, inject, Input, Renderer2 } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NavigationEnd, Router } from '@angular/router';
import { interval, Subject, Subscription, takeUntil } from 'rxjs';

@Directive({
  selector: '[appMyRouterLink]',
  standalone: true
})
export class MyRouterLinkDirective  {
  @Input({ alias: 'appMyRouterLink'})
  classToApply: string = ''

  router = inject(Router)
  el = inject(ElementRef<HTMLElement>)
  renderer2 = inject(Renderer2)

  constructor() {
   this.router.events
     .pipe(
       takeUntilDestroyed()
     )
      .subscribe(ev => {
        if (ev instanceof NavigationEnd) {
          const currentPath = ev.url;
          const routerLinkPath = this.el.nativeElement.getAttribute('routerLink')

          if (currentPath.includes(routerLinkPath)) {
            this.renderer2.addClass(
              this.el.nativeElement,
              this.classToApply
            )
          } else {
            this.renderer2.removeClass(
              this.el.nativeElement,
              this.classToApply
            )
          }

        }
      })
  }

}
