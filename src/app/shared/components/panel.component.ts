import { Component, inject } from '@angular/core';
import { WrapperComponent } from './wrapper.component';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [],
  template: `
    <em>
      
      <ng-content></ng-content>
    </em>
  `,
  styles: ``
})
export class PanelComponent {

  wrapper = inject(WrapperComponent, { optional: true })

  ngOnInit() {
    if(this.wrapper) {
      console.log(this.wrapper.title)
    }
  }
}
