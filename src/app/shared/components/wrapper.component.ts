import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-wrapper',
  standalone: true,
  imports: [],
  template: `
    <h1>{{title}}</h1>
    <ng-content></ng-content>
  `,
  styles: ``
})
export class WrapperComponent {
  @Input() title = ''
}
