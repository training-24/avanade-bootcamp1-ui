import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-hero',
  standalone: true,
  imports: [],
  template: `
    <section class="bg-white dark:bg-gray-900">
      <div class="py-8 px-4 mx-auto max-w-screen-xl text-center lg:py-16">
        <h1 class="mb-4 text-4xl font-extrabold tracking-tight leading-none text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
          {{ title }}
        </h1>
        <p class="mb-8 text-lg font-normal text-gray-500 lg:text-xl sm:px-16 lg:px-48 dark:text-gray-400">
          {{description}}
        </p>
        
        <ng-content></ng-content>
        
        <div class="flex flex-col gap-3 sm:flex-row sm:justify-center sm:space-y-0">

          @for (button of buttons; track $index) {
            <a [href]="button.url" class="inline-flex justify-center items-center py-3 px-5 text-base font-medium text-center text-white rounded-lg bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:focus:ring-blue-900">
              {{button.label}}
            </a>
          }
          
        </div>
      
      </div>
    </section>
  `,
  styles: ``
})
export class HeroComponent {
  @Input()
  title: string | undefined;

  @Input() description: string | undefined

  @Input() button1Label: string | undefined
  @Input() button1Url: string | undefined

  @Input() buttons: { label: string, url: string }[] = []

}
