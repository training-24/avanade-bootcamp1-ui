import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [],
  template: `

    <div class="w-full p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <a href="#">
        <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
          {{title}}  
        </h5>
      </a>

      <ng-content></ng-content>
      
    </div>
      
  `,
  styles: ``
})
export class CardComponent {
  @Input() title: string | undefined
}
