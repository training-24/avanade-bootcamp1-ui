import { Routes } from '@angular/router';
import Demo1Component from './features/demo1.component';

export const routes: Routes = [
  { path: 'demo1', component: Demo1Component},
  { path: 'demo2', loadComponent: () => import('./features/demo2.component')},
  { path: 'demo3', loadComponent: () => import('./features/demo3.component')},
  { path: 'demo4', loadComponent: () => import('./features/demo4/demo4.component')},
  { path: 'demo5', loadComponent: () => import('./features/demo5/demo5.component')},
  { path: 'demo6', loadComponent: () => import('./features/demo6/demo6.component')},
  { path: '', redirectTo: 'demo1', pathMatch: 'full'}
];
