import { NgIf } from '@angular/common';
import { AfterViewInit, Component, ElementRef, inject, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { PanelComponent } from '../shared/components/panel.component';
import { WrapperComponent } from '../shared/components/wrapper.component';
import { IfLoggedDirective } from '../core/auth/if-logged.directive';
import { StopPropagationDirective } from '../shared/directives/stop-propagation.directive';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [
    NgIf,
    PanelComponent,
    WrapperComponent,
    StopPropagationDirective,
    IfLoggedDirective
  ],
  template: `
    <!--
    <div *ngIf="visible; else tpl ">VISIBLE CONTENT</div>
    <button (click)="visible = !visible">toggle</button>-->
    <h1>Visible only when logged</h1>
    <div *appIfLogged="'admin'">
      ADMIN PANEL
    </div>
  `,
  styles: ``
})
export default class Demo2Component {
}
