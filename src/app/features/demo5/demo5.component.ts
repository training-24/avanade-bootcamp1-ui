import { Component, signal } from '@angular/core';
import { CardComponent } from '../../shared/components/card.component';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [
    CardComponent
  ],
  template: `
    <p>
      demo5 works!
    </p>

    @defer (on interaction) {
      <app-card/>
    } @placeholder {
      <button>Load List</button>
    } @loading {
      <div>Rendering ...</div>
    } @error {
      <div>Failed to load chunk</div>
    }

    <button (click)="toggle()">{{isVisible()}}</button>
    
    @defer (when isVisible() ) {
      @if(isVisible()) {
        <app-card title="ciao" />
      }
    }
    
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    @defer (on interaction) {
      <app-card title="ciao" />
    } @placeholder {
       <div>placeolder</div>
    }

    @defer (on viewport) {
      <app-card title="ciaone" />
    }@placeholder {
      <div role="status" class="max-w-sm animate-pulse">
        <div class="h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"></div>
        <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px] mb-2.5"></div>
        <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"></div>
        <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[330px] mb-2.5"></div>
        <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[300px] mb-2.5"></div>
        <div class="h-2 bg-gray-200 rounded-full dark:bg-gray-700 max-w-[360px]"></div>
        <span class="sr-only">Loading...</span>
      </div>
    }

  `,
  styles: ``
})
export default class Demo5Component {
  isVisible = signal(false)
  toggle() {
    this.isVisible.update(prev => !prev)
  }
}
