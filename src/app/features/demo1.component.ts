import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { BorderDirective } from '../shared/directives/border.directive';
import { HighlightDirective } from '../shared/directives/highlight.directive';
import { MarginDirective } from '../shared/directives/margin.directive';
import { PadDirective } from '../shared/directives/pad.directive';
import { StopPropagationDirective } from '../shared/directives/stop-propagation.directive';
import { UrlDirective } from '../shared/directives/url.directive';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    RouterLink,
    PadDirective,
    HighlightDirective,
    FormsModule,
    MarginDirective,
    BorderDirective,
    UrlDirective,
    StopPropagationDirective
  ],
  template: `
    <button 
      (click)="value = value + 2"
    >btn {{value}}</button>
    
    <hr>
      
    <button
      url="http://www.google.com"
      (click)="doSomething()"
      appPad="xl"
      [appBorder]="value"
    >
      Lorem <span appHighlight>ipsum </span> dolor sit
    </button>

    <form (submit)="doSomething()">
      <input type="text" name="username" placeholder="username">
      <button type="submit">ssubmit</button>
    </form>
    
    
    <div class="bg-red-500 p-4" 
         (click)="parentClick()">
      parent
      <div 
        class="bg-orange-400" 
        (click)="childClick()"
        appStopPropagation
      >
        child
      </div>
    </div>
    
  `,
})
export default class Demo1Component {
  value = 1;

  parentClick() {
    console.log('parentClick')
  }
  childClick() {
    console.log('childClick')
  }
  doSomething() {
    console.log('from compo')
  }
}

