import { NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormatToPipe } from '../shared/pipes/format-to.pipe';
import { MultiplyPipe } from '../shared/pipes/multiply.pipe';

@Component({
  selector: 'app-demo3',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MultiplyPipe,
    NgIf,
    FormatToPipe
  ],
  template: `
    <p>
      demo3 works!
       {{value | multiply}} {{multiply()}}
    </p>
    
    
    <button (click)="doSomething()">
      do something
    </button>
    
   
    {{byte | formatTo: format}}
    
    <div #ref ngIf="condition"></div>
    
    <!--
    @for(user of (users() | filterBy: 'M')) {
      <div></div>
    }-->
  `,
  styles: ``
})
export default class Demo3Component {
  @ViewChild('ref', { static: true}) panel: undefined;
  value = 10;
  byte = 1000000
  format: 'mb' | 'gb' = 'mb'


  ngOnInit() {

  }
  multiply() {
    console.log('multiply')
    return this.value * 2
  }

  doSomething()
  {
    this.value = 5
    // this.byte = 20000000
    this.format = 'gb'
  }
}
