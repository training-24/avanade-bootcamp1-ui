import { Dialog } from '@angular/cdk/dialog';
import { CdkDrag, CdkDragDrop, CdkDragPlaceholder, CdkDropList, moveItemInArray } from '@angular/cdk/drag-drop';
import { OverlayModule } from '@angular/cdk/overlay';
import { Component, inject } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatTooltip } from '@angular/material/tooltip';
import { ModalClientsComponent } from './components/modal-clients.component';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [MatButton, MatTooltip, OverlayModule, CdkDropList, CdkDragPlaceholder, CdkDrag],

  template: `
    <p>
      demo6 works!
    </p>

    <div cdkDropList class="example-list" (cdkDropListDropped)="drop($event)">
      @for (movie of movies; track movie) {
        <div class="example-box" cdkDrag>
          <div class="example-custom-placeholder" *cdkDragPlaceholder></div>
          {{movie}}
        </div>
      }
    </div>
    
    
    <button
      mat-raised-button
      (click)="openDialog()"
      matTooltip="This is a tooltip!">Open Dialog</button>

    <button (click)="isOpen = !isOpen" type="button" cdkOverlayOrigin 
            #trigger="cdkOverlayOrigin">
      {{isOpen ? "Close" : "Open"}}
    </button>

    <!-- This template displays the overlay content and is connected to the button -->
    <ng-template
      cdkConnectedOverlay
      [cdkConnectedOverlayOrigin]="trigger"
      [cdkConnectedOverlayOpen]="isOpen"
    >
      <ul class="example-list">
        <li>Item 1</li>
        <li>Item 2</li>
        <li>Item 3</li>
      </ul>
    </ng-template>
  `,
  styles: `
    .example-list {
      width: 500px;
      max-width: 100%;
      border: solid 1px #ccc;
      min-height: 60px;
      display: block;
      background: white;
      border-radius: 4px;
      overflow: hidden;
    }

    .example-box {
      padding: 20px 10px;
      border-bottom: solid 1px #ccc;
      color: rgba(0, 0, 0, 0.87);
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      box-sizing: border-box;
      cursor: move;
      background: white;
      font-size: 14px;
    }

    .cdk-drag-preview {
      box-sizing: border-box;
      border-radius: 4px;
      box-shadow: 0 5px 5px -3px rgba(0, 0, 0, 0.2),
      0 8px 10px 1px rgba(0, 0, 0, 0.14),
      0 3px 14px 2px rgba(0, 0, 0, 0.12);
    }

    .cdk-drag-animating {
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }

    .example-box:last-child {
      border: none;
    }

    .example-list.cdk-drop-list-dragging .example-box:not(.cdk-drag-placeholder) {
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }

    .example-custom-placeholder {
      background: #ccc;
      border: dotted 10px blue;
      min-height: 60px;
      transition: transform 250ms cubic-bezier(0, 0, 0.2, 1);
    }
  
  `
})
export default class Demo6Component {
  dialog = inject(Dialog)
  isOpen = false;

  openDialog(): void {
    const dialogRef = this.dialog.open<string>(ModalClientsComponent, {
      width: '250px',
      data: {name: 'fabio' },
    });

    dialogRef.closed.subscribe(result => {
      console.log('The dialog was closed', result);
    });
  }

  movies = [
    'Episode I - The Phantom Menace',
    'Episode II - Attack of the Clones',
    'Episode III - Revenge of the Sith',
    'Episode IV - A New Hope',
    'Episode V - The Empire Strikes Back',
    'Episode VI - Return of the Jedi',
    'Episode VII - The Force Awakens',
    'Episode VIII - The Last Jedi',
    'Episode IX - The Rise of Skywalker',
  ];

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.movies, event.previousIndex, event.currentIndex);
  }
}
