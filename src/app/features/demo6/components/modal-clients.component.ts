import { DIALOG_DATA, DialogRef } from '@angular/cdk/dialog';
import { Component, inject, Inject } from '@angular/core';

@Component({
  selector: 'app-modal-clients',
  standalone: true,
  imports: [],
  template: `
    <div class="bg-white rounded-xl">
    <h1>Hi {{data.name}}</h1>
    <div>
      <button (click)="dialogRef.close('pipppo')">OK</button>
      <button (click)="dialogRef.close()">Cancel</button>
    </div>
    </div>
  `,
  styles: ``
})
export class ModalClientsComponent {
  dialogRef: DialogRef<string> = inject(DialogRef);

  constructor(
    // public dialogRef: DialogRef<string>,
    @Inject(DIALOG_DATA)
    public data: { name: string },
  ) {}
}
