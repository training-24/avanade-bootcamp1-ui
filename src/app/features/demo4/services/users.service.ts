import { HttpClient } from '@angular/common/http';
import { inject, Injectable, signal } from '@angular/core';
import { delay } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { User } from '../../../model/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  http = inject(HttpClient)
  users = signal<User[]>([])
  error = signal<boolean>(false)

  loadUsers() {
    this.http.get<User[]>(`${environment.api}/users`)
      .subscribe({
        next: res => {
          this.users.set(res)
        },
        error: () => {
          this.error.set(true)
        }
      })
  }

  add(formValue: Pick<User, 'name' | 'username'>) {
    this.error.set(false);
    this.http.post<User>(`${environment.api}/users`, formValue)
      .subscribe(newUser => {
        // this.users.set([...this.users(), newUser])
        this.users.update(users => [...users, newUser])
      })
  }

  /**
   * Example optimistic update (grezzo 😅!)
   * @param user
   */
  deleteUser(user: User) {
    const rollback = structuredClone(this.users());
    this.users.update(users => users.filter(u => u.id !== user.id))

    const index = this.users().findIndex(u => u.id === user.id)

    this.error.set(false);
    this.http.delete(`${environment.api}/usersX/${user.id}`).pipe(delay(1000))
      .subscribe({
        error: () => {
          this.users.set(rollback)
          this.error.set(true);

        }
      })
  }
}
