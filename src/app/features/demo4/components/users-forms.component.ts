import { Component, EventEmitter, inject, Output } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-forms',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  template: `
    <form [formGroup]="form" (submit)="addHandler()">
      <input type="text" formControlName="name">
      <input type="text" formControlName="username">
      <button type="submit" [disabled]="form.invalid">ADD</button>
    </form>
  `,
  styles: ``
})
export class UsersFormsComponent {
  @Output() addUser = new EventEmitter<Pick<User, 'name' | 'username'>>()

  fb = inject(FormBuilder)
  form = this.fb.nonNullable.group({
    name: ['', [Validators.required]],
    username: ['', [Validators.required]],
  })

  addHandler() {
    this.addUser.emit({
      name: this.form.value.name!,
      username: this.form.value.name!,
    })
  }
}
