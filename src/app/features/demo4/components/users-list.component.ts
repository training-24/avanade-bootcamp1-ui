import { Component, computed, EventEmitter, input, Input, Output, signal } from '@angular/core';
import { User } from '../../../model/user';
import { UsersListItemComponent } from './users-list-item.component';

@Component({
  selector: 'app-users-list',
  standalone: true,
  imports: [UsersListItemComponent],
  template: `
    @for(user of users(); track user.id) {
      <app-users-list-item 
        [user]="user"
        (deleteUser)="deleteUser.emit($event)"
      /> 
    }
  `,
})
export class UsersListComponent {
  //@Input() users: User[] = []
  users = input<User[]>([])
  @Output() deleteUser = new EventEmitter<User>()

}
