import { Component, computed, input, Input } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'app-users-summary',
  standalone: true,
  imports: [],
  template: `
    <div>
      total user {{totalUsers()}} 
      total ids: {{totalSumIds()}}
    </div>
  `,
  styles: ``
})
export class UsersSummaryComponent {
  users = input.required<User[]>()
  totalUsers = computed(() => this.users().length)
  totalSumIds = computed(() => this.users().reduce((acc, item) => {
    return acc + item.id;
  }, 0))
}
