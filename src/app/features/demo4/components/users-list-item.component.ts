import { JsonPipe } from '@angular/common';
import { Component, EventEmitter, input, Output, signal } from '@angular/core';
import { User } from '../../../model/user';
import { IfLoggedDirective } from '../../../core/auth/if-logged.directive';

@Component({
  selector: 'app-users-list-item',
  standalone: true,
  imports: [
    JsonPipe,
    IfLoggedDirective
  ],
  template: `
    <div>
      {{user().name}} - {{user().username}}
      <button (click)="deleteUser.emit(user())">Delete</button>
      <button (click)="toggle()" *appIfLogged="'admin'">Toggle</button>

      @if(opened()) {
        <div>
          <pre>{{user() | json}}</pre>
        </div>
      }

    </div>
  `,
  styles: ``
})
export class UsersListItemComponent {
  @Output() deleteUser = new EventEmitter<User>()

  user = input.required<User>()
  opened = signal(false)

  toggle() {
    this.opened.update(isOpen => !isOpen)
  }
}
