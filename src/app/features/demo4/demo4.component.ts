import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, OnInit, signal } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { User } from '../../model/user';
import { AlertComponent } from '../../shared/components/alert.component';
import { CardComponent } from '../../shared/components/card.component';
import { HeroComponent } from '../../shared/components/hero.component';
import { UsersFormsComponent } from './components/users-forms.component';
import { UsersListComponent } from './components/users-list.component';
import { UsersSummaryComponent } from './components/users-summary.component';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    JsonPipe,
    ReactiveFormsModule,
    HeroComponent,
    CardComponent,
    AlertComponent,
    UsersListComponent,
    UsersSummaryComponent,
    UsersFormsComponent
  ],
  template: `
    <app-hero
      title="Fabio Biondi"
      description="123 Here at Flowbite we focus on markets where technology, innovation, and capital can unlock long-term value and drive economic growth."
      [buttons]="[
       { label: 'one', url: 'https://jsonplaceholder.typicode.com'},
       { label: 'two', url: 'https://jsonplaceholder.typicode.com'},
       { label: 'three', url: 'https://jsonplaceholder.typicode.com'},
      ]"
    > 
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto consequatur consequuntur dolores ex expedita fugit iusto labore nam porro, praesentium quam, qui tempora temporibus. Aperiam consequatur culpa eveniet ipsum numquam!
    </app-hero>
    
    <app-card title="User list">
      @if(usersService.error()) {
        <app-alert>ahia errore server</app-alert>
      }
      <app-users-summary [users]="usersService.users()" />
      <app-users-forms (addUser)="usersService.add($event)" />
      <app-users-list [users]="usersService.users()" (deleteUser)="usersService.deleteUser($event)"/>
    </app-card>
    
  `,
})
export default class Demo4Component implements OnInit {
  usersService = inject(UsersService)

  ngOnInit() {
      this.usersService.loadUsers()
  }
}
