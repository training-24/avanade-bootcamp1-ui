import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { NavbarComponent } from './core/components/navbar.component';
import { IfLoggedDirective } from './core/auth/if-logged.directive';
import { MyRouterLinkDirective } from './shared/directives/my-router-link.directive';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, RouterLinkActive, MyRouterLinkDirective, IfLoggedDirective, NavbarComponent],
  template: `
    
    <app-navbar />
    <div class="max-w-screen-lg mx-6 lg:mx-auto">
      <router-outlet />
    </div>
  `,
  styles: [],
})
export class AppComponent {
}
